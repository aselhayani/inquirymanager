﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inquiry_Manger
{
    public partial class frm_Branch   : DevExpress.XtraEditors.XtraForm
    {
        public frm_Branch()
        {
            InitializeComponent();
        }

        private void frm_Branch_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = Data.MainDataSet.Tables[1];
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            Data.SaveMainDataFile();
        }
    }
}
