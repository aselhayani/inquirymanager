﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inquiry_Manger
{
    public class Data
    {
        public static DataSet MainDataSet = new DataSet("MainDataSet");
        public static void CreatTables()
        {
            MainDataSet.Tables.Add("Customers");
            MainDataSet.Tables[0].Columns.Add("Code", typeof(string));
            MainDataSet.Tables[0].Columns.Add("Name", typeof(string));
            MainDataSet.Tables[0].Columns.Add("Branch", typeof(string));
            MainDataSet.Tables.Add("Branches");
            MainDataSet.Tables[1].Columns.Add("Code", typeof(string));
            MainDataSet.Tables[1].Columns.Add("Name", typeof(string));
            MainDataSet.Tables.Add("ReportType");
            MainDataSet.Tables[2].Columns.Add("Code", typeof(string));
            MainDataSet.Tables[2].Columns.Add("Name", typeof(string));
            MainDataSet.Tables.Add("Officar");
            MainDataSet.Tables[3].Columns.Add("Code"  , typeof(string));
            MainDataSet.Tables[3].Columns.Add("Name"  , typeof(string));
            MainDataSet.Tables[3].Columns.Add("Branch", typeof(string));
            MainDataSet.Tables.Add("Inquiry");
            MainDataSet.Tables[4].Columns.Add("Inquiry Code", typeof(string));
            MainDataSet.Tables[4].Columns.Add("Customer", typeof(string));
            MainDataSet.Tables[4].Columns.Add("Branch", typeof(string));
            MainDataSet.Tables[4].Columns.Add("Officar", typeof(string));
            MainDataSet.Tables[4].Columns.Add("entry user", typeof(string));
            MainDataSet.Tables[4].Columns.Add("Status", typeof(string));
            MainDataSet.Tables[4].Columns.Add("Descreption", typeof(string));
            MainDataSet.Tables[4].Columns.Add("Notes", typeof(string));
            MainDataSet.Tables.Add("Status");
            MainDataSet.Tables[5].Columns.Add("Code", typeof(string));
            MainDataSet.Tables[5].Columns.Add("Name", typeof(string));
            MainDataSet.Tables.Add("Users");
            MainDataSet.Tables[6].Columns.Add("Name", typeof(string));
            MainDataSet.Tables[6].Columns.Add("Code", typeof(string));
            MainDataSet.Tables[6].Columns.Add("Creator", typeof(string));
            MainDataSet.Tables[6].Columns.Add("IsAdmin", typeof(string));

        }
        public static void SaveMainDataFile()
        {
            foreach (DataRow  row in MainDataSet.Tables[6].Rows)
            {
                row[2] = Cryptography.Encrypt(row[2].ToString());
                row[0] = Cryptography.Encrypt(row[0].ToString());
                row[1] = Cryptography.Encrypt(row[1].ToString());
                row[3] = Cryptography.Encrypt(row[3].ToString());
            }
            MainDataSet.WriteXml(Main.DataFilePath);
        }
        public static void ReadMainDataFile()
        {
            if(File.Exists (Main.DataFilePath))
            {
                MainDataSet.Clear();
                MainDataSet.ReadXml(Main.DataFilePath);
                foreach (DataRow row in MainDataSet.Tables[6].Rows)
                {
                    row[0] = Cryptography.Decrypt(row[0].ToString());
                    row[1] = Cryptography.Decrypt(row[1].ToString());
                    row[2] = Cryptography.Decrypt(row[2].ToString());
                    row[3] = Cryptography.Decrypt(row[3].ToString());
                }
            }

        }

    }
}
