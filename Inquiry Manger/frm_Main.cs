﻿using DevExpress.XtraBars.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Inquiry_Manger
{
    public partial class frm_Main : DevExpress.XtraEditors.XtraForm
    {
        public frm_Main()
        {
            InitializeComponent();
            SkinHelper.InitSkinGallery(skinRibbonGalleryBarItem1);

        }
        int pos;
        bool IsNew; 
        private void Form1_Load(object sender, EventArgs e)
        {
          
           btn_AddUsers.Enabled = (Main.Isadmin);
            RefreshData();
            btn_New.PerformClick();
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\GridLayout.xml"))
                gridView1.RestoreLayoutFromXml(AppDomain.CurrentDomain.BaseDirectory + "\\GridLayout.xml");


        }
        void RefreshData()
        {
            gridControl1.DataSource = Data.MainDataSet.Tables[4];

            lkp_Customer.Properties.DataSource = Data.MainDataSet.Tables[0];
            lkp_Customer.Properties.ValueMember = "Code";
            lkp_Customer.Properties.DisplayMember = "Name";

            lkp_Branch.Properties.DataSource = Data.MainDataSet.Tables[1];
            lkp_Branch.Properties.ValueMember = "Code";
            lkp_Branch.Properties.DisplayMember  = "Name";

            lkp_Officar.Properties.DataSource = Data.MainDataSet.Tables[3];
            lkp_Officar.Properties.ValueMember = "Code";
            lkp_Officar.Properties.DisplayMember = "Name";

            RepositoryItemLookUpEdit RepoCustomer = new RepositoryItemLookUpEdit();
            RepoCustomer.DataSource = Data.MainDataSet.Tables[0];
            RepoCustomer.ValueMember = "Code";
            RepoCustomer.DisplayMember = "Name";

            RepositoryItemLookUpEdit RepoBranch = new RepositoryItemLookUpEdit();
            RepoBranch.DataSource = Data.MainDataSet.Tables[1];
            RepoBranch.ValueMember = "Code";
            RepoBranch.DisplayMember = "Name";

            RepositoryItemLookUpEdit RepoOfficar = new RepositoryItemLookUpEdit();
            RepoOfficar.DataSource = Data.MainDataSet.Tables[3];
            RepoOfficar.ValueMember = "Code";
            RepoOfficar.DisplayMember = "Name";

            RepositoryItemMemoEdit memoEdit = new RepositoryItemMemoEdit();
            memoEdit.WordWrap = true;

            RepositoryItemMemoEdit memoEdit2 = new RepositoryItemMemoEdit();
            memoEdit.WordWrap = true;

            RepositoryItemLookUpEdit RepoStatus = new RepositoryItemLookUpEdit();
            RepoStatus.DataSource = Data.MainDataSet.Tables[5];
            RepoStatus.ValueMember = "Code";
            RepoStatus.DisplayMember = "Name";


            gridView1.Columns[7].ColumnEdit = memoEdit;
            gridView1.Columns[6].ColumnEdit = memoEdit2;
            gridView1.Columns[1].ColumnEdit = RepoCustomer;
            gridView1.Columns[2].ColumnEdit = RepoBranch;
            gridView1.Columns[3].ColumnEdit = RepoOfficar;
            gridView1.Columns[6].ColumnEdit = RepoStatus;


            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] { RepoCustomer, RepoBranch, RepoOfficar, memoEdit , RepoStatus });



        }

        private void btn_New_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            IsNew = true;
            dateEdit1.DateTime = DateTime.Now;
            lkp_Officar.EditValue = 0;
            lkp_Customer.EditValue = 0;
            lkp_Branch.EditValue = 0;
            memoEdit_Notes.Text = "";
            memoEdit_Desc .Text = "";
            txt_Code.Text  = GetNewCode("Inquiry").ToString();
        }
        Int64  GetNewCode(string tableName)
        {

            var max = Data.MainDataSet.Tables[tableName].Compute("MAX([Inquiry Code])", "");
            string code = "";

            if (max == DBNull.Value) return 1;
            foreach (char chr in max.ToString().ToCharArray())
            {
                if (char.IsDigit(chr))
                    code += chr;
            }
            if (code == "") code = DateTime.Now.ToString("yyyyMMddhhmm");
            return Convert.ToInt64(code)+1;
        }

        private void btn_Customers_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Customers().ShowDialog();
            RefreshData();
        }

        private void btn_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int last = 0;
            if (IsNew) 
            {
                Data.MainDataSet.Tables[4].Rows.Add();
                last = Data.MainDataSet.Tables[4].Rows.Count-1;
            }
            if (!IsNew)
            {
                last = pos;
            }
            Data.MainDataSet.Tables[4].Rows[last][0] =txt_Code.Text ;
            Data.MainDataSet.Tables[4].Rows[last][1] =lkp_Customer .EditValue ;
            Data.MainDataSet.Tables[4].Rows[last][2] =lkp_Branch.EditValue ;
            Data.MainDataSet.Tables[4].Rows[last][3] =lkp_Officar .EditValue ;
            Data.MainDataSet.Tables[4].Rows[last][4] = Main.CurrentUser;
            Data.MainDataSet.Tables[4].Rows[last][5] =cb_Status.SelectedIndex;
            Data.MainDataSet.Tables[4].Rows[last][6] =memoEdit_Desc .Text ;
            Data.MainDataSet.Tables[4].Rows[last][7] =memoEdit_Notes .Text;
            Data.MainDataSet.AcceptChanges();
            pos = last;
            IsNew = false;
            Data.SaveMainDataFile();
        }
        void laod (int handeler)
        {
            txt_Code.Text             =Data.MainDataSet.Tables[4].Rows[handeler][0] .ToString()  ;
            lkp_Customer.EditValue    =Data.MainDataSet.Tables[4].Rows[handeler][1]   ;
            lkp_Branch.EditValue      =Data.MainDataSet.Tables[4].Rows[handeler][2]   ;
            lkp_Officar.EditValue     =Data.MainDataSet.Tables[4].Rows[handeler][3]   ;
            cb_Status.SelectedIndex   =(Data.MainDataSet.Tables[4].Rows[handeler][5] == DBNull.Value )?0: Convert.ToInt32( Data.MainDataSet.Tables[4].Rows[handeler][5]);
            memoEdit_Desc.Text        =Data.MainDataSet.Tables[4].Rows[handeler][6].ToString();
            memoEdit_Notes.Text       =Data.MainDataSet.Tables[4].Rows[handeler][7].ToString();
            pos = handeler;
            IsNew = false;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0 )
                laod(gridView1.FocusedRowHandle);
        }

        private void btn_Branches_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Branch().ShowDialog();
            RefreshData();

        }

        private void skinRibbonGalleryBarItem1_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
        }

        private void skinRibbonGalleryBarItem1_GalleryItemClick(object sender, DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs e)
        {
            
        }

        private void Form1_StyleChanged(object sender, EventArgs e)
        {
            Debug.Print(this.LookAndFeel.SkinName);
        }

        private void btn_Officars_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Officers().ShowDialog();
            RefreshData();
        }

        private void btn_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(DevExpress.XtraEditors.XtraMessageBox.Show(text:"Are you Sure you want to delete this inquiry ?",caption: "Warning",buttons: MessageBoxButtons.YesNo ,icon: MessageBoxIcon.Exclamation  ,defaultButton: 0) == DialogResult.Yes)
            {
                gridView1.DeleteRow(pos);
                Data.SaveMainDataFile();
                IsNew = true;
                pos = 0;

            }
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void memoEdit_Desc_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void memoEdit_Desc_KeyDown(object sender, KeyEventArgs e)
        {
            MemoEdit memo = sender as MemoEdit;
            if(e.KeyCode == Keys.D && e.Modifiers == Keys.Alt )
            {
                var insertText = DateTime.Now.ToString("yyyy-MM-dd hh:mm tt ");
                var selectionIndex = memo.SelectionStart;
                memo.Text = memo.Text.Insert(selectionIndex, insertText);
                memo.SelectionStart = selectionIndex + insertText.Length;

            }
        }

        private void frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
           
            gridView1.SaveLayoutToXml(AppDomain.CurrentDomain.BaseDirectory + "\\GridLayout.xml");
        }

        private void btn_AddUsers_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(Main.Isadmin)
            AddUser.ShowLogin();
        }

        private void btn_Print_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridControl1.ShowRibbonPrintPreview();

        }

        private void gridView1_PrintInitialize(object sender, DevExpress.XtraGrid.Views.Base.PrintInitializeEventArgs e)
        {
            PrintingSystemBase system = e.PrintingSystem as PrintingSystemBase;
            system.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A5;
            system.PageSettings.Landscape = true;
            system.PageSettings.Margins.Left = 25;
            system.PageSettings.Margins.Right = 25;
            system.PageSettings.Margins.Top = 25;
            system.PageSettings.Margins.Bottom = 25;
        }

        private void btn_ChangeUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditUsers.ShowLogin();

        }
    }
}
