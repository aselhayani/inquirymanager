﻿using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inquiry_Manger
{
    public partial class frm_Customers : DevExpress.XtraEditors.XtraForm
    {
        public frm_Customers()
        {
            InitializeComponent();
        }

        private void frm_Customers_Load(object sender, EventArgs e)
        {
            RefreshData();
        }
        void RefreshData()
        {
            gridControl1.DataSource = Data.MainDataSet.Tables[0];
            RepositoryItemLookUpEdit RepoBranch = new RepositoryItemLookUpEdit();
            RepoBranch.DataSource = Data.MainDataSet.Tables[1];
            RepoBranch.ValueMember = "Code";
            RepoBranch.DisplayMember = "Name";
            gridView1.Columns[2].ColumnEdit = RepoBranch;
            gridControl1.RepositoryItems.AddRange(new RepositoryItem[] {   RepoBranch  });

        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            Data.SaveMainDataFile();
        }
    }
}
